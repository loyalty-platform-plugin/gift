import React from 'react'
import {BrowserRouter as Router, Route} from 'react-router-dom'
import Home from './Home'
import Campaign from './Campaign'
import './gift.css'


const App = () => (
  <Router>
    <div>
      <Route exact path="/gift" component={Home}/>
      <Route path="/gift/campaign/:name/:campaignId" component={Campaign}/>
    </div>
  </Router>
)
export default App