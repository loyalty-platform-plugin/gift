import React from 'react'
import Cookies from 'js-cookie'
import {Link} from 'react-router-dom'


const campaigns = [
{
	name : 'Детский мир',
	description : 'Для участия в акции необходимо совершить покупку в сети магазинов "Детский мир" на сумму от 1000 рублей и предьявить промо-код кассиру.',
},{
	name : 'Michelin',
	description : 'При покупке комплекта зимних шин Michelin получаете шиномонтаж в подарок'
}]

const getRandomNumber = (min,max) => Math.floor(Math.random() * (max - min)) + min
const getCode = (path) => Cookies.get(path)
const setCode = (path) => Cookies.set(path, getRandomNumber(10000, 15000))


const Code = ({code}) => (
	<div className='code'>
		Ваш промо-код: 
		<span className='button value'>
			{code}
		</span>
	</div>
)

const GetCode = ({onGetCode}) => (
	<div
		className='button card-action'
		onClick={onGetCode}
	>
		Получить скидку
	</div>
)

const CardContent = ({code, onGetCode}) => {
	return code ? <Code code={code} /> : <GetCode onGetCode={onGetCode} />
}


class Campaign extends React.Component {
	constructor(props) {
		super(props)
		const path = props.match.params.campaignId
		this.state = {
			code : getCode(path),
			path : path,
			campaign : campaigns[props.match.params.name]
		}
		this.generateCode = this.generateCode.bind(this)
	}

	generateCode() {
		setCode(this.state.path)
		this.setState(prevState => ({
			code : getCode(this.state.path)
		}));
	}

	render() { 
		return (
		<div className="content">
			<div className='card'>
				<h1 className='card-header'>
					{this.state.campaign.name}
				</h1>
				<div className='card-body'>
					<div className='terms'>
						{this.state.campaign.description}
					</div>
					<CardContent
						code={this.state.code}
						onGetCode={this.generateCode}
					/>
				</div>
			</div>
			<Link className="link" to="/gift">Назад</Link>
		</div>
	)}
}

export default Campaign