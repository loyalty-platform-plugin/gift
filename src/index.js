import React from 'react'
import {render} from 'react-dom'
import App from './App'

render(
	<App />,
	document.getElementById('loyalty_platform_gift_plugin')
);
