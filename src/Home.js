import React from 'react'
import {Link} from 'react-router-dom'

const Home = () => (
	<div className="content">
		<h2 className='card-header'>Выгодные покупки вместе с<br /><a href='http://loyaltyplatform.ru/'>Loyalty Platform</a></h2>
		<div className='list-item'>
			<Link className="link" to="/gift/campaign/0/ff021cde-4964-4db5-a7c1-f33ffddad540">Скидка в "Детском мире"</Link>
		</div>
		<div className='list-item'>
			<Link className="link"to="/gift/campaign/1/711eec4d-c158-449c-a426-00fefc6e5416">Шиномонтаж от Michelin</Link>
		</div>
	</div>
)

export default Home